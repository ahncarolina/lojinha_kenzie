from flask import Blueprint, request
from app.models import User
from app.models import db
from typing import List
from http import HTTPStatus
from sqlalchemy.exc import IntegrityError


bp_users = Blueprint('bp_users', __name__, url_prefix='/users')


def serialize_user(user: User) -> dict:
    return {
        'id': user.id,
        'name': user.name,
        'surname': user.surname
    }


def serialize_user_list(user_list: List[User]) -> List[dict]:
    return [serialize_user(user) for user in user_list]


@bp_users.route('/')
def list_all():
    users = User.query.all()
    user_dicts = serialize_user_list(users)

    return {'data': user_dicts}, HTTPStatus.OK


@bp_users.route('/', methods=['POST'])
def create():
    user_data = request.get_json()
    user = User(
        name=user_data["name"],
        surname=user_data['surname'],
        document=user_data['document']
    )

    try:
        db.session.add(user)
        db.session.commit()
        return 'created', HTTPStatus.CREATED
    except IntegrityError:
        return {'msg': 'Bad request'}, HTTPStatus.BAD_REQUEST
